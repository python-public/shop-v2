# Приложение для электронной коммерции
## Django Projects

<ul><b>Приложение для электронной коммерции</b> полнофункциональный онлайн-магазин
<li>Созданы модели товаров</li>
<li>Создана корзина с помощью Django sessions</li>
<li>Создание пользовательских контекстных процессоров</li>
<li>Управление заказами клиентов</li>
<li>Отправляйте асинхронные уведомления с помощью Celery и RabbitMQ</li>
<li>Просмотр списка Celery с помощью Flower</li>
<li>Интегрирован платежный сервис Stripe</li>
<li>Настроен webhook для получения ответов от Stripe</li>
<li>Кастомизирована админка добавлен функционал</li>
<li>Генерация CSV-файлов заказов</li>
<li>Генерация счетов фактуры в PDF с помощью Weasyprint</li>
<li>Создан функционал скидок</li>
<li>Интеграция скидок с Stripe</li>
<li>Создан блок рекомендаций с помощью Redis</li>
</ul>

## Запуск приложения
<ol>
<li>pip install -r requirements.txt</li>
<li>docker pull redis</li>
<li>docker run -it --rm --name redis -p 6379:6379 redis</li>
<li>redis-cli</li>
<li>docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.11-management</li>
<li>Необходимо установить stripe-cli для вашей OS https://stripe.com/docs/stripe-cli и войти "stripe login"</li>
<li>После установки stripe-cli мы выполняем команду "stripe listen --forward-to localhost:8000/payment/webhook/"</li>
<li>Запуск команды из каталога проекта "celery -A myshop worker -l info"</li>
<li>python manage.py makemigrations</li>
<li>python manage.py migrate</li>
<li>python manage.py runserver</li>
</ol>